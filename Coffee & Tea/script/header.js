const headerScrollElm = document.getElementById("header-scroll");
const notificationScrollElm = document.getElementById("notification-scroll");
const headerMenuScrollELm = document.getElementById("header-menu-scroll");
const headerMenuScrollLogoElm = document.getElementById(
    "header-menu-scroll-logo"
);

window.addEventListener("scroll", () => {
    const scrolled = window.scrollY;
    const MaScroll = Math.ceil(scrolled);

    if (MaScroll > 200) {
        headerScrollElm.classList.add("headerScrolled");
        headerMenuScrollELm.classList.add("headerMenuScrolled");
        headerMenuScrollLogoElm.classList.add("headerMenuScrolledLogo");
        notificationScrollElm.style.visibility = "hidden";
        // headerMenuScrollELm.style.top = "-110px";
    } else {
        headerScrollElm.classList.remove("headerScrolled");
        headerMenuScrollELm.classList.remove("headerMenuScrolled");
        headerMenuScrollLogoElm.classList.remove("headerMenuScrolledLogo");
        notificationScrollElm.style.visibility = "visible";
        // headerMenuScrollELm.style.top = "-15px";
    }
});

$(".drawer .bx.bx-search-alt").click(function() {
    $(".drawer .child").slideDown();
});

$(".drawer .bx.bx-x").click(function() {
    $(".drawer .child").slideUp();
});

$(".search-menu .bx.bx-search").click(function() {
    $("#header-search").slideDown();
});

$("#header-search .bi.bi-x").click(function() {
    $("#header-search").slideUp();
});

$(".header-search-item input").click(function() {
    $(".header-search-list").slideDown("fast");
});

$(".product-disabled-next").click(function() {
    $("#trendingProducts .owl-next").click();
});