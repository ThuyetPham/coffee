new WOW().init();

$(".banner .owl-carousel").owlCarousel({
    loop: true,
    margin: 10,
    nav: false,
    // autoplay: true,
    // autoplayTimeout: 6000,
    // autoplayHoverPause: true,
    responsive: {
        0: {
            items: 1,
        },
        992: {
            items: 1,
        },
        1200: {
            items: 1,
        },
    },
});

$(".banner .owl-carousel-blog").owlCarousel({
    loop: true,
    margin: 10,
    nav: false,
    // autoplay: true,
    // autoplayTimeout: 3000,
    // autoplayHoverPause: true,
    dots: false,
    responsive: {
        0: {
            items: 1,
        },
        992: {
            items: 1,
        },
        1200: {
            items: 1,
        },
    },
});

$(".owl-four").owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: false,
            autoplay: true,
        },
        600: {
            items: 2,
            nav: false,
            autoplay: true,
        },
        1000: {
            items: 3,
            nav: false,
        },
        1400: {
            items: 4,
            nav: false,
        },
    },
});

$(".owl-homeFeatured").owlCarousel({
    loop: true,
    margin: 20,
    nav: false,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: false,
            dots: true,
            autoplay: true,
            stagePadding: 50,
        },
        600: {
            items: 2,
            nav: false,
            dots: true,
            autoplay: true,
            stagePadding: 50,
        },
        1000: {
            items: 3,
            nav: false,
            dots: true,
            autoplay: true,
            stagePadding: 50,
        },
        1200: {
            items: 4,
            nav: false,
            dots: false,
        },
    },
});

$(".owl-six").owlCarousel({
    loop: true,
    margin: 10,
    nav: false,
    dots: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: false,
            dots: true,
            autoplay: true,
        },
        600: {
            items: 1,
            nav: false,
            dots: true,
            autoplay: true,
        },
        1000: {
            items: 1,
            nav: false,
            dots: true,
            autoplay: true,
        },
        1920: {
            items: 1,
            nav: false,
            dots: true,
        },
    },
});

$(".owl-seven").owlCarousel({
    loop: true,
    margin: 10,
    nav: false,
    dots: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: false,
            dots: true,
            autoplay: true,
        },
        600: {
            items: 1,
            nav: false,
            dots: true,
            autoplay: true,
        },
        1000: {
            items: 2,
            nav: false,
            dots: true,
            autoplay: true,
        },
        1920: {
            items: 3,
            nav: false,
            dots: true,
        },
    },
});

$(".product-carousel").owlCarousel({
    loop: false,
    margin: 0,
    nav: false,
    dots: false,
    autoplay: false,
    URLhashListener: true,
    startPosition: "URLHash",
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: false,
        },
        600: {
            items: 1,
            nav: false,
        },
        1000: {
            items: 1,
            nav: false,
        },
        1920: {
            items: 1,
            nav: false,
        },
    },
});

$(document).ready(function() {
    $("#myInput").on("keyup", function() {
        document.getElementById("search-drop").classList.toggle("show");
        var value = $(this).val().toLowerCase();
        $("#myList .list-group-item").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });
    });
    $(".dropSortby").click(function() {
        $(".dropSortby").toggleClass("colorChange");
        $(".dropSortby .fa-chevron-down").toggleClass("rotate");
        $(".menu-sortby").toggleClass("showSB");
        $(".menu-sortby > li").click(function() {
            $(".dropSortby > p").text($(this).text());
            $(".menu-sortby").removeClass("showSB");
        });
    });
    $(".dropdownCate").click(function() {
        $(".silder-cate").slideToggle("slow");
        $(".dropdownCate").toggleClass("colorChange");
        $(".dropdownCate .fa-chevron-up").toggleClass("rotate");
    });
    $(".dropdownTags").click(function() {
        $(".slide-tags").slideToggle("slow");
        $(".dropdownTags").toggleClass("colorChange");
        $(".dropdownTags .fa-chevron-up").toggleClass("rotate");
    });
    $(".dropdownColor").click(function() {
        $(".menu-color").slideToggle("slow");
        $(".dropdownColor").toggleClass("colorChange");
        $(".dropdownColor .fa-chevron-up").toggleClass("rotate");
    });

    $(".dropdownSize").click(function() {
        $(".menu-size").slideToggle("slow");
        $(".dropdownSize").toggleClass("colorChange");
        $(".dropdownSize .fa-chevron-up").toggleClass("rotate");
    });

    $(".dropdownBrands").click(function() {
        $(".slide-brands").slideToggle("slow");
        $(".dropdownBrands").toggleClass("colorChange");
        $(".dropdownBrands .fa-chevron-up").toggleClass("rotate");
    });
    $(".dropdownPrice").click(function() {
        $(".menu-price").slideToggle("slow");
        $(".dropdownPrice").toggleClass("colorChange");
        $(".dropdownPrice .fa-chevron-up").toggleClass("rotate");
    });
    $(".dropPosts").click(function() {
        $(".menu-posts").toggleClass("showPosts");
        $(".dropPosts").toggleClass("colorChange");
        $(".dropPosts .fa-chevron-up").toggleClass("rotate");
    });
});

const url = new URL(window.location.href);
const search_params = new URLSearchParams(url.search);
var rangeSlider = $(".price-range"),
    minamount = $("#minamount"),
    maxamount = $("#maxamount"),
    min = parseInt(search_params.get("price_min")) || 0,
    max = parseInt(search_params.get("price_max")) || 200;

$("#minamount").val(min.toLocaleString("vi"));
$("#maxamount").val("$" + max.toLocaleString("vi"));

rangeSlider.slider({
    range: true,
    min: 0,
    step: 1,
    max: 200,
    values: [min, max],
    slide: function(event, ui) {
        minamount.val("$" + ui.values[0].toLocaleString("vi"));
        maxamount.val("$" + ui.values[1].toLocaleString("vi"));
    },
});

function add(ths, sno) {
    for (var i = 1; i <= 5; i++) {
        var cur = document.getElementById("star" + i);
        cur.className = "far fa-star";
    }
    for (var i = 1; i <= sno; i++) {
        var cur = document.getElementById("star" + i);
        if (cur.className == "far fa-star") {
            cur.className = "fa fa-star";
        }
    }
}

let count = 0;
const countEl = document.getElementById("count");

// ............................
function plus() {
    count++;
    countEl.value = count;
    document.getElementById("plus").style = "color: var(--red);";
    document.getElementById("minus").style = "color: var(--dark);";

    document.getElementById("cssBuyNow").style =
        "background:var(--red); color:var(#ffffff);";
    document.getElementById("cssAddToCart").style = "color: var(--red)";
    document.getElementById("cssAddToCartBoder").style =
        "border: 1px solid var(--red)";
}

function minus() {
    if (count > 0) {
        count--;
        countEl.value = count;
        document.getElementById("minus").style = "color:var(--red);";
        document.getElementById("plus").style = "color:var(--dark);";

        document.getElementById("cssBuyNow").style =
            "background:var(--red); color:var(#ffffff);";
        document.getElementById("cssAddToCart").style = "color:var(--red)";
        document.getElementById("cssAddToCartBoder").style =
            "border: 1px solid var(--red)";
    }

    if (count === 0) {
        document.getElementById("minus").style = "color:var(--dark);";
        document.getElementById("plus").style = "color:var(--dark);";
        document.getElementById("cssBuyNow").style =
            "background:var(--dark); color:var(#ffffff);";
        document.getElementById("cssAddToCart").style = "color(--dark)";
        document.getElementById("cssAddToCartBoder").style =
            "border: 1px solid var(--dark)";
    }
}

// product

$(".nav-item.size-guide").click(function() {
    $("#tabSize").slideToggle("slow");
    $("#dropNavSize").toggleClass("cssColor");
    $("#dropSize").toggleClass("rotate");
});

$(".nav-item.info").click(function() {
    $("#tabInFo").slideToggle("slow");
    $("#dropNavInfo").toggleClass("cssColor");
    $("#dropInfo").toggleClass("rotate");
});